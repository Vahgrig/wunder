//
//  WunderAppTests.swift
//  WunderAppTests
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import XCTest
import MapKit
@testable import WunderApp

class WunderAppTests: XCTestCase {
    
    
    func testCarsFromMarkers() {
        var carsInfoArray = Array<Car>()
        self.GetPlaceModel { (markers, error) in
            if error == nil {
                if let validCarInfoArray = markers?.placemarks {
                    for carInfo in validCarInfoArray {
                        let car = Car(carInfoModel: carInfo)
                        carsInfoArray.append(car)
                    }
                }
                XCTAssertEqual(carsInfoArray.count, 423)
                XCTAssertEqual(self.testConfigMaxDistance(cars: carsInfoArray), 2986280)
            } else {
            }
        }
    }
    

    func testFilterCars() {
        self.GetPlaceModel { (markers, error) in
            
            let goodCarsSettings = SettingsState(isTenkm: false,
                                                 isThreekm: false,
                                                 isGoodCars: true,
                                                 isNormalCars: false)
            
            let goodCarsInThreeKMSettings = SettingsState(isTenkm: false,
                                                          isThreekm: true,
                                                          isGoodCars: true,
                                                          isNormalCars: false)
            
            let allCarsSettings = SettingsState(isTenkm: false,
                                                isThreekm: false,
                                                isGoodCars: false,
                                                isNormalCars: false)
            
            let settingsArray = [goodCarsSettings, goodCarsInThreeKMSettings, allCarsSettings]
            var index = 0
            for settingsState in settingsArray {
                
            var filteredCars = self.annotationsFromMarkers(markers: markers!)
            
            if settingsState.isGoodCars {
                filteredCars = filteredCars.filter {$0.carCondition == .good}
            }
            
            if settingsState.isNormalCars {
                filteredCars = filteredCars.filter {$0.carCondition == .normal}
            }
            
            if settingsState.isThreekm {
                filteredCars = self.validCarsInDistance(distance: 3000, filteredCars: filteredCars, settings: settingsState)
            }
            
            if settingsState.isTenkm {
                filteredCars = self.validCarsInDistance(distance: 10000, filteredCars: filteredCars, settings: settingsState)
            }
                if index == 0 {
                    XCTAssertEqual(filteredCars.count, 301)
                } else if index == 1 {
                    XCTAssertEqual(filteredCars.count, 0)
                } else if index == 2 {
                    XCTAssertEqual(filteredCars.count, 423)
                }
                index += 1
            }
        }
    }
    
    func validCarsInDistance(distance: Double, filteredCars: Array<Car>, settings: SettingsState) -> Array<Car> {
        var validCarsArray = Array<Car>()
        let userLocation = CLLocation(latitude: 40.20166111, longitude: 44.49077226)
        for car in filteredCars {
            let carLoaction = CLLocation(latitude: car.coordinate.latitude, longitude: car.coordinate.longitude)
            if carLoaction.distance(from: userLocation) < distance {
                validCarsArray.append(car)
            }
        }
        return validCarsArray
    }

    
    func testConfigMaxDistance(cars: Array<Car>) -> Int {
        let userLocation = CLLocation(latitude: 40.20166111, longitude: 44.49077226)
        if let maxDistCar = cars.max(by: { ($0.coordinate.locationFromCoordinate().distance(from: userLocation) < $1.coordinate.locationFromCoordinate().distance(from: userLocation)) }) {
             return Int(maxDistCar.coordinate.locationFromCoordinate().distance(from: userLocation))
        }
       return 0
    }
    
    func GetPlaceModel(completion: @escaping (PlacMakerModel?, Error?) -> Void) {
        
        let currentBundle = Bundle(for: type(of: self))
        if let path = currentBundle.path(forResource: "locations", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let placmakerModel = try JSONDecoder().decode(PlacMakerModel.self, from: data)
                completion(placmakerModel, nil)
            } catch {
                completion(nil,error)
            }
        }
    }

    func annotationsFromMarkers(markers: PlacMakerModel) -> Array<Car> {
        var carsInfoArray = Array<Car>()
        if let validCarInfoArray = markers.placemarks {
            for carInfo in validCarInfoArray {
                let car = Car(carInfoModel: carInfo)
                carsInfoArray.append(car)
            }
        }
        return carsInfoArray
    }
}
