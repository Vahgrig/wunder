//
//  PlaceMakerModel.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct PlacMakerModel: Codable {
    var placemarks: Array<CarInfoModel>?
}
