//
//  Car.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation
import MapKit
import Contacts

enum CarCondition {
    case good
    case normal
    case none
}

class Car: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var vin: String?
    var imageName: String?
    var fuel: Decimal?
    var carInfo: String?
    var carCondition: CarCondition?
    
    init(carInfoModel: CarInfoModel) {
        self.title = carInfoModel.name
        self.carInfo = " exterior: " + (carInfoModel.exterior ?? "Unknown") + " \n interior:  " + (carInfoModel.interior ?? "Unknown")
        self.subtitle = (carInfoModel.address ?? "") + (self.carInfo ?? "")
        self.vin = carInfoModel.vin
        self.fuel = carInfoModel.fuel
        if carInfoModel.exterior == "GOOD" && carInfoModel.interior == "GOOD" {
            self.carCondition = .good
        } else if carInfoModel.exterior == "UNACCEPTABLE" && carInfoModel.interior == "GOOD" {
            self.carCondition = .normal
        } else {
            self.carCondition = .none
        }
        self.coordinate = CLLocationCoordinate2D()
        if let coordinates = carInfoModel.coordinates, coordinates.count > 2 {
             self.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(coordinates[1]), longitude: coordinates[0])
        }
        super.init()
    }
    
    // Annotation right callout accessory opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle ?? ""]
        let placemark = MKPlacemark(coordinate: self.coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    
}
