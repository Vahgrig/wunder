//
//  CarInfoModel.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct CarInfoModel: Codable {
    let address: String?
    let coordinates: Array<Double>?
    let engineType: String?
    let exterior: String?
    let fuel: Decimal?
    let interior: String?
    let name: String?
    let vin: String?
}
