//
//  ApiClient.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation

class Server: NSObject {
    
    let urlString = "https://s3-us-west-2.amazonaws.com/wunderbucket/locations%2Ejson"
    
    func getLocationData(completion: @escaping (PlacMakerModel?, Error?) -> Void) {
        
        let url = URL(string: urlString)
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil
        
        let session = URLSession.init(configuration: config)
        
        session.dataTask(with: (url)!) { (data, response, error) in
            
            if error != nil {
                completion(nil, error)
                return
            }
            do {
                let placMakerModel = try JSONDecoder().decode(PlacMakerModel.self, from: data!)
                completion(placMakerModel, nil)
            } catch {
                completion(nil, error)
            }
            }.resume()
    }
    
}
