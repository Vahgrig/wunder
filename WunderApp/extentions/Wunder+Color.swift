//
//  Color.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/12/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    @objc static var wunderRed: UIColor {
        return #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
    }
    @objc static var wunderGreen: UIColor {
        return #colorLiteral(red: 0.6108884215, green: 0.7784385681, blue: 0.2793673575, alpha: 1)
    }
}
