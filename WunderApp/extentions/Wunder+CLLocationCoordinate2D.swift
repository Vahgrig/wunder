//
//  Wunder+CLLocationCoordinate2D.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation
import MapKit

extension CLLocationCoordinate2D {
    
    func isEqualToCoordinate(coordinate: CLLocationCoordinate2D) -> Bool {
        return self.latitude == coordinate.latitude && self.longitude == coordinate.longitude
    }
    
    func locationFromCoordinate() -> CLLocation {
       return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
    
}
