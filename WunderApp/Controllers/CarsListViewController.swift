//
//  CarsListViewController.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import UIKit

class CarsListViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingPageView: UIView!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var loadingLabel: UILabel!
    
    let keyboardHeight: CGFloat = 224
    var controller = CarsListController()
    var carsArray = Array<Car>()
    var searchedCarsArray = Array<Car>()
    var selectedCar: Car?
    
    
    // View LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedCar = self.selectedCar, let index = carsArray.index(where: { $0.vin == selectedCar.vin }) {
            carsArray[index].imageName = nil
            self.selectedCar = nil
        }
    }
    
    func configure() {
        searchBar.barTintColor = .white
        searchBar.searchBarStyle = .prominent
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        self.tableView.keyboardDismissMode = .onDrag

        for view in searchBar.subviews {
            for subview in view.subviews {
                if subview .isKind(of: UITextField.self) {
                    let textField: UITextField = subview as! UITextField
                    textField.backgroundColor = .white
                }
            }
        }
        getCarData()
    }
    
    func getCarData() {
        controller.carsFromMarkers { (cars, error) in
            if error == nil && cars != nil {
                self.carsArray = cars!
                self.searchedCarsArray = self.carsArray
                DispatchQueue.main.async {
                    self.loadingPageView.isHidden = true
                    self.tableView.reloadData()
                }
            } else {
                // error handling
                let alert = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .alert)
                alert.message = error?.localizedDescription
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                DispatchQueue.main.async {
                    self.retryButton.isHidden = false
                    self.loadingLabel.isHidden = true
                }
            }
        }
    }
    
    //MARK: - Button Actions

    @IBAction func retryServerCallPressed(_ sender: UIButton) {
        getCarData()
    }
    
    @IBAction func viewMapPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "Map", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? CarsMapViewController
        
        if let selectedCar = self.selectedCar {
            vc?.selectedAnnotation = selectedCar
            if let index = carsArray.index(where: { $0.vin == selectedCar.vin }) {
                carsArray[index].imageName = "selected_car"
            }
            vc?.controller.selectedCar = selectedCar
        }
        vc?.controller.carsInfoArray = carsArray
    }
}

extension CarsListViewController: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedCarsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath as IndexPath) as! CarCell
        let car = searchedCarsArray[indexPath.row]
        cell.carImageView.image = #imageLiteral(resourceName: "selected_car")
        cell.carNameLabel.text = car.title
        cell.carInfoLabel.text = car.carInfo
        cell.fuelProgressView.progress = Float(truncating: NSDecimalNumber(decimal: car.fuel ?? 0)) / controller.maxFuel
        return cell
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCar = searchedCarsArray[indexPath.row]
        self.performSegue(withIdentifier: "Map", sender: nil)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

extension CarsListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchedCarsArray = carsArray
        } else {
            let prefixArray = carsArray.filter {($0.title?.hasPrefix(searchText))!}
            let containArray = carsArray.filter {(($0.title?.containsIgnoringCase(find: searchText))! && !prefixArray.contains($0))}
            searchedCarsArray = prefixArray + containArray
        }
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
}
