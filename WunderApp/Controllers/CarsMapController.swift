//
//  CarsMapController.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation
import MapKit

struct SettingsState {
    var isTenkm = false
    var isThreekm = false
    var isGoodCars = false
    var isNormalCars = false
}

class CarsMapController: NSObject {
    
    var carsInfoArray = Array<Car>()
    var selectedCar: Car?
    var userLocation: CLLocation?
    var maxDistance: Double?
    var settingsState = SettingsState()
    var filteredCars = Array<Car>()
    var isFilteredByCondition: Bool {return settingsState.isGoodCars || settingsState.isNormalCars}
    var isFilteredByDistance: Bool {return settingsState.isTenkm || settingsState.isThreekm}
    
    func annotationsFromMarkers(markers: PlacMakerModel) -> Array<Car> {
        if let validCarInfoArray = markers.placemarks {
            for carInfo in validCarInfoArray {
                let car = Car(carInfoModel: carInfo)
                carsInfoArray.append(car)
            }
        }
        return carsInfoArray
    }
    
    func configMaxDistance() {
        guard let userLocation = self.userLocation else { return }
        if maxDistance == nil {
            let maxDistCar = carsInfoArray.max { ($0.coordinate.locationFromCoordinate().distance(from: userLocation) < $1.coordinate.locationFromCoordinate().distance(from: userLocation)) }
            maxDistance = maxDistCar?.coordinate.locationFromCoordinate().distance(from: userLocation)
        }
    }
    
    func filterCars() {
        
        filteredCars = carsInfoArray
        
        if self.settingsState.isGoodCars {
            filteredCars = filteredCars.filter {$0.carCondition == .good}
        }
        if self.settingsState.isNormalCars {
            filteredCars = filteredCars.filter {$0.carCondition == .normal}
        }
        if self.settingsState.isThreekm {
            filteredCars = validCarsInDistance(distance: 3000)
        }
        if self.settingsState.isTenkm {
            filteredCars = validCarsInDistance(distance: 10000)
        }
        
    }
    
    func validCarsInDistance(distance: Double) -> Array<Car> {
        var validCarsArray = Array<Car>()
        guard let userLocation = self.userLocation else { return Array<Car>() }
        var cars = Array<Car>()
        if isFilteredByCondition {
            cars = filteredCars
        } else {
            cars = carsInfoArray
        }
        for car in cars {
            let carLoaction = CLLocation(latitude: car.coordinate.latitude, longitude: car.coordinate.longitude)
            if carLoaction.distance(from: userLocation) < distance {
                validCarsArray.append(car)
            }
        }
        return validCarsArray
    }
    
    func distanceFromSelectedAnnotation(annotation: MKAnnotation) -> Double {
        let selectedLocation = CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)
        if let userLocation = self.userLocation {
            return userLocation.distance(from: selectedLocation)
        }
        return 0
    }
    
}
