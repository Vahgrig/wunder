//
//  CarsListController.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation
import UIKit

class CarsListController: NSObject {
    
    var maxFuel: Float = 0
    func carsFromMarkers(completion: @escaping (Array<Car>?, Error?) -> Void) {
        var carsInfoArray = Array<Car>()
        Server().getLocationData { (markers, error) in
            if error == nil {
                if let validCarInfoArray = markers?.placemarks {
                    for carInfo in validCarInfoArray {
                        let car = Car(carInfoModel: carInfo)
                        carsInfoArray.append(car)
                    }
                }
                completion(carsInfoArray, nil)
                self.maxFuelFromCars(cars: carsInfoArray)
            } else {
                completion(nil, error)
            }
        }
    }
    
    func maxFuelFromCars(cars: Array<Car>) {
        let carWithMaxFuel = cars.max { ($0.fuel! < $1.fuel!) }
        self.maxFuel = Float(truncating: NSDecimalNumber(decimal: (carWithMaxFuel?.fuel)!))
    }
    
}
