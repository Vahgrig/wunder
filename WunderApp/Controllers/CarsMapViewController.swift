//
//  CarsMapViewController.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class CarsMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var settingStateButton: UIButton!
    @IBOutlet weak var goodCarButton: UIButton!
    @IBOutlet weak var normCarButton: UIButton!
    @IBOutlet weak var tenkmButton: UIButton!
    @IBOutlet weak var threekmButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var mapSettingsHeightConstreint: NSLayoutConstraint!
    
    var controller = CarsMapController()
    var selectedAnnotation: MKAnnotation?
    var regionRadius: CLLocationDistance = 3000
    var isSettingsViewVisible = true
    let locationManager = CLLocationManager()
    let settingsViewHeight: CGFloat = 130
    let tenKmString = "10km"
    let threeKmString = "3km"
    let goodCarString = "Good Cars"
    let normCarString = "Norm Cars"
    
    //MARK: View Lifcicle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInitialState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func configureInitialState() {
        
        mapView.register(CarAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        configureMapAnnotations()
    }
    
    func configureMapAnnotations() {
        if let selectedCar = controller.selectedCar {
            mapView.removeAnnotations(mapView.annotations)
            mapView.addAnnotation(selectedCar)
        } else {
            mapView.addAnnotations(controller.carsInfoArray)
        }
    }
    
    func configureInitialRadius() {
        if let selectedAnnotation = self.selectedAnnotation {
            regionRadius = 2 * controller.distanceFromSelectedAnnotation(annotation: selectedAnnotation)
            distanceLabel.isHidden = false
            distanceLabel.text = "\(Int(regionRadius / 2000))km"
        } else {
            controller.configMaxDistance()
            regionRadius = 2 * (controller.maxDistance ?? regionRadius)
        }
    }
    
    //MARK: Gesture and Button Actions
    
    @IBAction func panHandled(_ sender: UIPanGestureRecognizer) {
        let point = sender.velocity(in: self.view)
        if point.y > 0 && isSettingsViewVisible {
            isSettingsViewVisible = false
            configSettingsView()
        }
    }
    
    func configSettingsView() {
        var settingsHeight: CGFloat = 0
        if isSettingsViewVisible {
            settingsHeight = settingsViewHeight
        }
        self.mapSettingsHeightConstreint.constant = settingsHeight
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.settingStateButton.transform = self.settingStateButton.transform.rotated(by: .pi)
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if selectedAnnotation != nil {
            mapView.deselectAnnotation(selectedAnnotation!, animated: true)
        }
    }
    
    @IBAction func settingStatePressed(_ sender: UIButton) {
        isSettingsViewVisible = !isSettingsViewVisible
        configSettingsView()
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nearByButtonPressed(_ sender: UIButton) {
        distanceLabel.isHidden = true
        if sender.titleLabel?.text == tenKmString {
            controller.settingsState.isTenkm = !controller.settingsState.isTenkm
            if controller.isFilteredByDistance {
                uncheckButtonName(buttonName: tenKmString)
            }
            regionRadius = 10000
            sender.backgroundColor = controller.settingsState.isTenkm ? UIColor.wunderRed : UIColor.wunderGreen
        } else if sender.titleLabel?.text == threeKmString {
            controller.settingsState.isThreekm = !controller.settingsState.isThreekm
            if controller.isFilteredByDistance {
                uncheckButtonName(buttonName: threeKmString)
            }
            regionRadius = 3000
            sender.backgroundColor = controller.settingsState.isThreekm ? UIColor.wunderRed : UIColor.wunderGreen
        } else if sender.titleLabel?.text == goodCarString {
            controller.settingsState.isGoodCars = !controller.settingsState.isGoodCars
            if controller.isFilteredByCondition {
                uncheckButtonName(buttonName: goodCarString)
            }
            sender.backgroundColor = controller.settingsState.isGoodCars ? UIColor.wunderRed : UIColor.wunderGreen
        } else if sender.titleLabel?.text == normCarString {
            controller.settingsState.isNormalCars = !controller.settingsState.isNormalCars
            if controller.isFilteredByCondition {
                uncheckButtonName(buttonName: normCarString)
            }
            sender.backgroundColor = controller.settingsState.isNormalCars ? UIColor.wunderRed : UIColor.wunderGreen
        } else {
            if controller.isFilteredByCondition {
                for buttonName in [normCarString, goodCarString] {
                    uncheckButtonName(buttonName: buttonName)
                }
            }
            if controller.isFilteredByDistance {
                for buttonName in [threeKmString, tenKmString] {
                    uncheckButtonName(buttonName: buttonName)
                }
            }
            regionRadius = 2 * (controller.maxDistance ?? 10000)
        }
        controller.filterCars()
        refreshMapviewWithAnnotations(annotations: controller.filteredCars)
    }
    
    func uncheckButtonName(buttonName: String) {
        if buttonName == threeKmString {
            controller.settingsState.isTenkm = false
            self.tenkmButton.backgroundColor = UIColor.wunderGreen
        } else if buttonName == tenKmString {
            controller.settingsState.isThreekm = false
            self.threekmButton.backgroundColor = UIColor.wunderGreen
        } else if buttonName == goodCarString {
            controller.settingsState.isNormalCars = false
            self.normCarButton.backgroundColor = UIColor.wunderGreen
        } else if buttonName == normCarString {
            controller.settingsState.isGoodCars = false
            self.goodCarButton.backgroundColor = UIColor.wunderGreen
        }
    }
    
    func refreshMapviewWithAnnotations(annotations: Array<MKAnnotation>) {
        guard let userLocation = controller.userLocation else { return }
        centerMapOnLocation(location: userLocation)
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(annotations)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
}

extension CarsMapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last, location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            controller.userLocation = location
            configureInitialRadius()
            centerMapOnLocation(location: location)
        }
    }
    
}

extension CarsMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let isEqual = selectedAnnotation?.coordinate.isEqualToCoordinate(coordinate: (view.annotation?.coordinate)!), isEqual {
            selectedAnnotation = nil
             distanceLabel.isHidden = true
            mapView.addAnnotations(controller.carsInfoArray)
            mapView.deselectAnnotation(view.annotation, animated: true)
        } else {
            selectedAnnotation = view.annotation
            if let selectedCoordinate = view.annotation?.coordinate {
                let removeAnnotations = controller.carsInfoArray.filter ({
                    return !$0.coordinate.isEqualToCoordinate(coordinate: selectedCoordinate)})
                let distance = controller.distanceFromSelectedAnnotation(annotation: view.annotation!)
                distanceLabel.text = "\(Int(distance / 1000))km"
                distanceLabel.isHidden = false
                mapView.removeAnnotations(removeAnnotations)
                mapView.addAnnotation(view.annotation!)
            }
        }
    }
    
}
