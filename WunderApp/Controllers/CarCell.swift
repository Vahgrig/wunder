//
//  CarCell.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/10/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import UIKit

class CarCell: UITableViewCell {
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carInfoLabel: UILabel!
    @IBOutlet weak var fuelProgressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fuelProgressView.trackTintColor = #colorLiteral(red: 0.9567713212, green: 0.9567713212, blue: 0.9567713212, alpha: 1)
    }
    
    override func prepareForReuse() {
        carImageView.image = nil
        carNameLabel.text = nil
        carInfoLabel.text = nil
        fuelProgressView.progress = 0
    }
    
}
