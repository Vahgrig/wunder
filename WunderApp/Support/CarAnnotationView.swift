//
//  File.swift
//  WunderApp
//
//  Created by Vahan Grigoryan on 10/11/18.
//  Copyright © 2018 Vahan Grigoryan. All rights reserved.
//

import Foundation
import MapKit

class CarAnnotationView: MKAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            guard let car = newValue as? Car else {return}
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            image = UIImage(named: car.imageName ?? "car")
        }
    }
}
